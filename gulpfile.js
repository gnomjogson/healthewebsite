var gulp = require('gulp'),
    compass = require('gulp-compass'),
    notify = require("gulp-notify"),
    livereload = require('gulp-livereload'),
    usemin = require('gulp-usemin'),
    replace = require('gulp-replace'),
    uglify = require('gulp-uglifyjs');
autoprefixer = require('gulp-autoprefixer'),
    less = require('gulp-less'),
    svgSprite = require('gulp-svg-sprite');


var config = {
    base_path: './library/'
}


// JS
var libs = [
    config.base_path +'js/vendor/modernizr.custom.min.js'
    ,config.base_path +'js/vendor/logger.min.js'
    ,config.base_path +'js/plugins.js'
    ,config.base_path +'js/menu.js'
    ,config.base_path +'js/main.js'
];

gulp.task('uglify', function() {
    gulp.src(libs)
        .pipe(uglify('app.min.js'))
        .pipe(gulp.dest(config.base_path + 'js/'))
});

gulp.task('compass', function() {
    gulp.src(config.base_path + 'scss/**/*.scss')
        .pipe(compass({
            config_file: './library/scss/config.rb',
            css: config.base_path + 'css',
            sass: config.base_path +'scss',
            font: config.base_path +'fonts',
            image: config.base_path +'images'
        })
            .on("error", notify.onError(function (error) {
                return "Error: " + error.message;
            }))
    )
        .pipe(autoprefixer({
            browsers: ['last 2 versions','ie 9','ie 8']
        }))
        .pipe(gulp.dest(config.base_path + 'css'))
        .pipe(notify("Done!!!!"));
});


gulp.task('watch', function() {
    var server = livereload.listen();
    gulp.watch(config.base_path + 'scss/**/*.scss', ['compass']);
    //gulp.watch(config.base_path + 'less/**/*.less', ['less']);

    //   gulp.watch('./source/*.*', ['minify']);
    gulp.watch(config.base_path + 'css/**/*.css').on('change',livereload.changed);
});

gulp.task('default', ['uglify', 'watch']);
