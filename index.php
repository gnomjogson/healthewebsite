<?php

$env = 'prod'; /*prod or dev*/
$url_shop = 'https://rainbow-unicorn.com/product/towel/';
$base_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_font_download = 'https://github.com/rainbowunicornstudio/healtheweb-typeface';
$pdf_download = $base_url . '/download/SpecimenHealTheWeb.pdf';
$description = 'Heal The Web is an experimental open source Typeface designed for everyone. Originally developed by Rainbow Unicorn & Jakub Kanior for Mozilla’s Internet Health Report 2020 the lettering resembles circuits, connections and networks through Geometric Shapes sticking together like an abstract Puzzle.';
$cachebust = '?v=1.018';

?>

<!doctype html>
<html class="no-js" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="<?php echo $description; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="Heal The Web — A typeface to heal the world">
  <meta property="og:type" content="website">
  <meta property="og:url" content="http://healtheweb.site">
  <meta property="og:image" content="http://healtheweb.site/library/img/social-og-healtheweb-1200x630.png">
  <meta property="og:description" content="<?php echo $description; ?>">

  <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" sizes="57x57" href="library/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="library/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="library/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="library/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="library/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="library/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="library/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="library/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="library/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="library/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="library/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="library/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="library/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="library/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="library/css/normalize.css">
  <link rel="stylesheet" href="library/css/style.css<?php echo $cachebust; ?>">

  <meta name="theme-color" content="#ffffff">
</head>

<body>
  <div class="loader"></div>
  <header class="global-header fixed-top">
    <div class="container">

        <nav class="navbar">
            <a href="<?php echo $base_url; ?>" class="logo fontsize-logo ss04 link-no-underline ticker">
                <span class="ticker-target">heal the web</span>
            </a>

            <div class="navbar-toggle fontsize-logo"></div>

            <ul class="topnav fontsize-30 d-block d-md-flex">
                <li>
                    <a href="#try" class="btn pulse topnav__try selectors">TRY</a>
                </li>
                <li>
                    <a href="#styles" class="btn selectors">STYLES</a>
                </li>
                <li>
                    <a href="#info" class="btn selectors">INFO</a>
                </li>
                <li>
                    <a href="#shop" class="btn selectors">SHOP</a>
                </li>
                <li>
                    <a href="#download" class="btn nav-download">GET IT!</a>
                </li>
            </ul>
        </nav>
    </div>
  </header>

  <!-- hero will stay in background always -->
  <div class="hero fullwidth fullheight d-flex justify-content-center align-items-center">
      <div class="hero-inner text-center fade-in">
          <span class="fontsize-400 htworld"></span>
      </div>
  </div>

  <div id="site-wrapper">
      <div class="container">
          <div id="try" class="grid-container grid-container__1 section-block mb-160 pt-80 nav-section">
              <div data-type="font-htweb" class="white-box grid-item grid-item-0 text-input-box">
                  <div class="white-box-inner">
                      <div class="d-block">
                          <a data-type="font-htweb" class="btn btn-rounded fontsize-30 active font-htweb btn-changefont mr-1" href="#" data-class="font-htweb">Heal the web A</a>
                          <a data-type="font-htworld" class="btn btn-rounded fontsize-30 font-htworld btn-changefont" href="#" data-class="font-htworld" >Heal the web B</a>
                      </div>
                      <div class="d-block my-5">
                          <span role="textbox" contenteditable id="text-input" class="font-htweb" onfocus="this.select();" onmouseup="return false;" class="user-input" type="text" style="font-size:90px;">What is your future planet pet?</span>
                      </div>
                      <div class="d-flex flex-wrap align-items-center justify-content-between slider-wrap">
                          <div class="styleset-nav">
                              <a href="#" class="btn btn-styleset fontsize-20 active" data-style="ss01">SS01</a>
                              <a href="#" class="btn btn-styleset fontsize-20" data-style="ss02">SS02</a>
                              <a href="#" class="btn btn-styleset fontsize-20" data-style="ss03">SS03</a>
                              <a href="#" class="btn btn-styleset fontsize-20" data-style="ss04">SS04</a>
                              <a href="#" class="btn btn-styleset fontsize-20" data-style="ss05">SS05</a>
                              <a href="#" class="btn btn-styleset fontsize-20" data-style="ss06">SS06</a>
                              <a href="#" class="btn btn-case fontsize-20 active" data-style="text-lowercase">aa</a>
                              <a href="#" class="btn btn-case fontsize-20" data-style="text-uppercase">AA</a>
                          </div>
                          <div class="range-slider">
                              <input class="range-slider__range" type="range" value="90" min="16" max="200">
                              <!--<span class="range-slider__value">30</span>-->
                          </div>
                      </div>

                  </div>
              </div>
              <div id="styles" class="white-box grid-item grid-item-1 nav-section">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">Heal the Web A</h3>
                      <p class="fontsize-90 lh-1 font-htweb mb-0">ABCDEFGHIJKLMNOPQRSTUVWXYZ</p>
                      <p class="fontsize-90 lh-1 font-htweb mb-5">abcdefghijklmnopqrstuvwxyz</p>

                      <h3 class="fontsize-20 font-htweb mb-4 ss01">ss01: Single Story a and g</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss01" ><span class="normal">agenda</span> → ag<span class="normal">enda</span></p>

                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4 ss02">ss02: Alternate Caps Set</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss02"> <span class="normal">REALITY</span> → REA<span class="normal">LITY</span> </p>

                      <h3 class="fontsize-20 font-htweb mb-4 ss03">ss03: Alternate Lower Case Single Story a and g</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss03"> <span class="normal">agenda</span> → ag<span class ="normal">enda</span> </p>

                      <h3 class="fontsize-20 font-htweb mb-4 ss04"> ss04: Alternate t With Extended Crossbar</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss04"> <span class="normal">Alternate</span> → <span class ="normal">Al</span>t<span class="normal">ernate</span> </p>

                      <h3 class="fontsize-20 font-htweb mb-4 ss05"> ss05: Alternate Lower Case Set</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss05"> <span class="normal">Alternate</span> → <span class ="normal">A</span>lt<span class ="normal">er</span>nat<span class="normal">e</span></p>

                      <h3 class="fontsize-20 font-htweb mb-4 ss06"> ss06: Straight Stroke Quotes, Comma and Semicolon</h3>
                      <p class="fontsize-60 font-htweb mb-5 ss06"> <span class="normal">DC, LA; „NY“</span> → <span class ="normal">DC</span>, <span class ="normal">LA</span>; „<span class ="normal">NY</span>“ </p>
                  </div>
              </div>
              <div class="white-box grid-item grid-item-2">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htworld mb-4">Heal the Web B</h3>
                      <p class="fontsize-90 lh-1 font-htworld mb-0">ABCDEFGHIJKLMNOPQRSTUVWXYZ</p>
                      <p class="fontsize-90 lh-1 font-htworld mb-5">abcdefghijklmnopqrstuvwxyz</p>

                      <h3 class="fontsize-20 font-htworld mb-4 ss01"> ss01: Odd Alternate Caps</h3>
                      <p class="fontsize-60 font-htworld mb-5 ss01" > <span class ="normal">ANGRY DOLL</span> → ANGRY D<span class ="normal">O</span>LL </p>

                      <h3 class="fontsize-20 font-htworld mb-4 ss02"> ss02: Comon Alternate Caps</h3>
                      <p class="fontsize-60 font-htworld mb-5 ss02" > <span class ="normal">MONT REALITY</span> → MONT REA<span class="normal">LI</span>TY </p>

                      <h3 class="fontsize-20 font-htworld mb-4 ss03"> ss03: Slap Serif Alternate Caps</h3>
                      <p class="fontsize-60 font-htworld mb-5 ss03" > <span class ="normal"> TARANTINO</span> → TA<span class ="normal">R</span>A<span class ="normal">N</span>TI<span class="normal">NO</span> </p>

                      <h3 class="fontsize-20 font-htworld mb-4 ss04"> ss04: Alternate Lowercase Glyphs</h3>
                      <p class="fontsize-60 font-htworld ss04"> <span class ="normal"> web reality</span>→ web rea<span class="normal">li</span>ty </p>
                  </div>
              </div>
              <a href="<?php echo $pdf_download; ?>" target="_blank" class="white-box grid-item grid-item-3 download-specimen">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">download specimen pdf</h3>
                      <div class="d-flex">
                          <div class="download-arrow">
                              <div class="download-arrow-inner">
                                  <svg version="1.1" id="Ebene_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                       width="127.5px" height="127.5px" viewBox="0 0 127.5 127.5" style="enable-background:new 0 0 127.5 127.5;" xml:space="preserve"
                                      >
                                        <style type="text/css">
                                            .st0{enable-background:new    ;}
                                        </style>
                                                                              <g class="st0">
                                                                                  <path d="M10.6,127.5L0,116.8L101.8,15H33.7V0h93.7v93.7h-15V25.6L10.6,127.5z"/>
                                                                              </g>
                                        </svg>
                              </div>
                          </div>
                      </div>
                  </div>
              </a>
          </div>
          <div class="grid-container grid-container__2  section-block mb-80">
              <div class="white-box grid-item-0">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">Numbers</h3>
                      <p class="fontsize-40 mb-5">0123456789 ½ ¼ ¾ ⅛ ⅜ ⅝ ⅞<br>⁰¹²³⁴⁵⁶⁷⁸⁹</p>

                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">WITH HUNDREDS OF EXTENDED LATIN GLYPHS</h3>
                      <div class="flip-element elem-1">
                          <p class="fontsize-40 font-htweb elem-1">àáâãäåǎǟāăąạấầẩẫắằẳẵặæǣḅɓćĉċčçƈďđḍḏḓɖɗðēėęȩěèéêḙëẹẻẽếềểễệəɛʒǯƒĝğġģḡǧɠĥħḥḫẖĩīįıɨìíîïǐỉịḯĵȷķǩḵƙĺļļḷḹḻḽľłḿṁṃṅṇṉṋńņņňŋñɲòóôõöǒøǿōőṍṑṓȫọỏốồổỗộơớờởỡợɔœþƥɋŕŗřṝṟɽɾśŝşșṣšʃßţťțŧṭṯṱẗƭʈùúûüǔǖǘǚǜũūŭůűṷųụủʉưứừửữựʋʌŵẃẅẏýÿŷȳỳỵỷỹƴẓẕźżž</p>
                          <p class="fontsize-40 font-htweb elem-2">ÀÁÂÃÄÅĀĂĄǍǞẰẲẴẶẠẢẦẨẪẮÆǢɃḄƁÇĆĈĊČƇĎÐḌḎḒƊĒĖĘÈÉÊËĚȨḘẸẺẼẾỀỂỄỆƎƏƐƷǮƑĜĞĠĢǤǦḠƓĤḤḪĦÌÍÎƗÏĮĨİǏỊḮỈĴĶǨḴƘĹĻĻĽŁḶḸḺḼḾṀṂŃŅŇŊÑṄṆṈṊƝÒÓÔÕÖǑØǾŌŐȪṌṐṒỌỎỐỒỔỖỘƠỚỜỞỠỢƆŒÞƤŔŖŘṜṞɌṢŚŜŞŠȘẞŢŤŦȚṬṮṰƬƮŨŪŬŮŰŲÙÚÛÜǓɄỤỦǕǗǙǛƯỨỪỬỮỰɅƲŴẀẄÝŶŸȲẎỲỴỶỸƳŹŻŽẒẔ</p>
                      </div>

                  </div>
              </div>
              <div class="white-box grid-item-1 glyphs-btn">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">Extended glyphs & symbols</h3>
                      <div class="glyphs-wrap">
                          <div class="glyph fontsize-glyph d-flex justify-content-center">
                              <span class="d-inline-block glyph-char" data-content="💩,◬,👁,◷,🪐,🌐,📶,🔍,☉,☆,☮,⚫,◼,⯅,⯆,🕱,🐭,🐱,◔,👻,♡,♥,❤,🍦,👍,👎,💀,☠,🖒,🖓, ,☻,😃,😅,😇,😍,😎,😐,😢,😶,🙁,🙃,🙂,🤮,🤯,🤬,₿,⧫"></span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="white-box grid-item-2">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">Symbols</h3>
                      <p class="fontsize-40 font-htworld">.,:;…-–—_!¡?¿·•*/#\‚„“”‘’
                          (){}[]«»‹›$¢€£¥₿⧫Ξ@&¶§©®™°|¦†‡•
                          +−×÷=≠><≥≤±≈~¬^∅∞∫∏∑√∂%‰№π
                          ↑↗→↘↓↙←↖↔↕
                          ●◊■▲▼△▽◬👁◷🪐🌐📶🔍☉☆☮
                          ⚫◼⯅⯆🕱🐭🐱◔👻♡♥❤🍦💩
                          👍👎💀☠🖒🖓 ☻😃😅😇😍😎😐😢
                          😶🙁🙃🙂🤮🤯🤬</p>
                  </div>
              </div>
              <div class="white-box grid-item-3">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4">Support for 93 languages</h3>
                      <p class="fontsize-12 lh-2">Afrikaans, Albanian, Asu, Basque, Bemba, Bena, Breton, Catalan, Chiga, Colognian, Cornish, Croatian, Czech, Danish, Dutch, Embu, English, Esperanto, Estonian, Faroese, Filipino, Finnish, French, Friulian, Galician, Ganda, German, Gusii, Hungarian, Inari Sami, Indonesian, Irish, Italian, Jola-Fonyi, Kabuverdianu, Kalenjin, Kamba, Kikuyu, Kinyarwanda, Latvian, Lithuanian, Lower Sorbian, Luo, Luxembourgish, Luyia, Machame, Makhuwa-Meetto, Makonde, Malagasy, Maltese, Manx, Meru, Morisyen, Northern Sami, North Ndebele, Norwegian Bokmål, Norwegian Nynorsk, Nyankole, Oromo, Polish, Portuguese, Quechua, Romanian, Romansh, Rombo, Rundi, Rwa, Samburu, Sango, Sangu, Scottish Gaelic, Sena, Serbian, Shambala, Shona, Slovak, Soga, Somali, Spanish, Swahili, Swedish, Swiss German, Taita, Teso, Turkish, Upper Sorbian, Uzbek (Latin), Volapük, Vunjo, Walser, Welsh, Western Frisian, Zulu.<br><br>
                      And many, many, manymany more...</p>
                  </div>
              </div>
          </div>

          <div id="info" class="section-block pt-80 mb-80 nav-section">
              <div class="white-box shrinked">
                  <div class="white-box-inner">
                      <h3 class="fontsize-20 text-uppercase font-htweb mb-4 mb-md-5">The story</h3>
                      <div class="font-htweb fontsize-30 px-1 py-2 px-md-5 py-md-3 the-story">
                          <p>The font started with a simple but big message: Heal the web with an experimental open-source Typeface designed for everyone. Originally developed by Rainbow Unicorn for Mozilla's <a class="background-hover mobile-underline" href="https://2020.internethealthreport.org/" target="_blank">Internet Health Report 2020</a>, the lettering resembles circuits, connections and networks through geometric shapes, sticking together like an abstract puzzle.</p>
                          <p>While preserving the original edgy character, the goal became to create an entire font with improved legibility and complex lettering for typographic experiments.</p>
                          <p><a href="#download" class="highlighted">Heal the Web A</a> is best used for body text with improved legibility, but still displays a very strong geometric edginess.</p>
                          <p><a href="#download" class="highlighted">Heal the Web B</a> stands out with its edgy typography and contains abstract Glyphs. It is best used as an all caps font, while the minuscules are a construction kit for further experiments. Unlike its other version, it consists of strictly geometric shapes without optical correction.</p>
                          <p>Both versions have collections of alternate Glyphs that originated from the in-between steps of the design process. They are accessible through OpenType stylistic sets.</p>
                          <p>The 'Heal the Web' font is licensed under the <a href="https://www.mozilla.org/en-US/MPL/2.0/FAQ/" target="_blank" class="mobile-underline">MPL-2.0 license</a>.</p>
                      </div>
                  </div>
              </div>
              <div class="white-box shrinked">
                  <div class="white-box-inner">
                      <h3 class="fontsize-23 text-uppercase font-htworld mb-4 mb-md-5">Credits</h3>
                      <p class="fontsize-40 font-htworld">Design: <a href="https://rainbow-unicorn.com" target="_blank" class="mobile-underline background-hover" >Rainbow Unicorn</a> & <a href="http://www.flexn.de/" target="_blank" class="mobile-underline background-hover">Jakub Kanior</a><br>
                          Production: <a href="http://www.flexn.de/" target="_blank" class="mobile-underline background-hover">Jakub Kanior</a></p>
                  </div>
              </div>
          </div>

          <!-- shop -->
          <div id="shop" class="section-block pt-80 mb-80 nav-section">
              <a class="d-block shop-teaser-wrap" href="<?php echo $url_shop; ?>" target="_blank">
                  <span class="sr-only">Buy the Heal the Web Towel</span>
                  <div class="shop-teaser-inner mobile"></div>
                  <div class="shop-teaser-inner desktop"></div>
                  <div class="shop-teaser-btn font-htworld text-uppercase fontsize-60 text-center">
                      Shop now
                  </div>
              </a>
          </div>
          <div id="download" class="section-block pt-80 pt-80">
              <a href="<?php echo $url_font_download; ?>" target="_blank" class="ticker btn-download-font fontsize-60 font-htworld" download>
                  download<br>
                  <span class="ticker-target">Heal The Web A & B </span><br>
                  on GitHub
              </a>
          </div>
      </div>

      <footer class="footer">
          <div class="container">
              <nav class="navbar">
                  <a href="https://rainbow-unicorn.com/imprint/" target="_blank" class="btn font-htworld fontsize-30 text-uppercase">
                      <span class="ticker"><span class="ticker-target">Imprint</span></span>
                  </a>
                  <div class="font-htworld fontsize-30 text-uppercase">
                      <a class="btn" href="mailto:help@healtheweb.site"><span class="ticker"><span class="ticker-target">help@healtheweb.site<span></span></a>
                      </a>
                  </div>
                  <a href="#" class="btn to-top-btn"><span class="sr-only">scroll back to top</span></a>
              </nav>
          </div>
      </footer>

  </div>

  <!-- We load JS files at the bottom of page -->
  <script src="library/js/vendor/jquery-3.2.1.min.js"></script>

  <?php if($env == 'dev'){ ?>
      <script src="library/js/vendor/modernizr.custom.min.js"></script>
      <script src="library/js/vendor/logger.min.js"></script>
      <script src="library/js/plugins.js<?php echo $cachebust; ?>"></script>
      <script src="library/js/slider.js<?php echo $cachebust; ?>"></script>
      <script src="library/js/menu.js<?php echo $cachebust; ?>"></script>
      <script src="library/js/main.js<?php echo $cachebust; ?>"></script>
  <?php } else { ?>
      <!-- When on production server, we only load the final minified app.min.js -->
      <script src="library/js/slider.js<?php echo $cachebust; ?>"></script>
      <script src="library/js/app.min.js<?php echo $cachebust; ?>"></script>
  <?php } ?>

  <script>
      var baseUrl = '<?php echo $base_url; ?>'
      var cachebust = '<?php echo $cachebust; ?>'
  </script>

  <script>
      jQuery(document).ready(function($) {
          window.controller = new Controller($);
          window.controller.init();
      });
  </script>

</body>

</html>
