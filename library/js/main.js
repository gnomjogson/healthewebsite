(function(window){

    Controller.prototype.constructor = Controller;
    Controller.prototype = {
        specimen_colors : [ '#E0BCFF', '#DEFFDD', '#efefef', '#0033CC', '#666666']
    };

    var $, ref, glypsArray, $body, glyph_index, $topNavItems, $changeFontButtons, $textInput, specimen_timer, currentSpecimenIndex,
        $header, $textTickers, ticker_timer, $currentTickerElement, currentTickerIndex, glyphs_timer,
        claims, claimStyles, claimsIndex, claim_timer, $mobileToggle, $topNav, blockRemove, glyphs;
    function Controller(){
        ref = this;
        $ = window.jQuery;

        Logger.useDefaults();
        Logger.setLevel(Logger.OFF);

        var browser = ref.getBrowser();

        $('body').addClass(browser.name.toLowerCase()).addClass('version-' + browser.version.toLowerCase());

    };

    Controller.prototype.init = function(){

        var json = window.baseUrl + "library/data/claims.json" + window.cachebust;
        Logger.log(json);
        $.getJSON(json, function(data){
            claims = data.claims;
            claimStyles = data.styles;
            claimsIndex = 0;
            ref.setClaim();
            claims.shift();
            ref.shuffleArray(claims);
            claimsIndex = ref.getRandomNumberBetween(0,claims.length-1);
        }).fail(function(){
            Logger.log("An error has occurred.");
        });

        $body = $('body');
        $header = $('.global-header');
        $topNavItems = $('.topnav a');
        $textInput = $('#text-input');
        $topNav = $('.topnav');
        $mobileToggle = $('.navbar-toggle');
        $mobileToggle.click(function(e){
            $header.toggleClass('mobile-nav-open');

        });

        /* on TAB key we activate accessibilty focus*/
        $(document).on('keydown', function(e) {

            if ( e.keyCode == 9){
                $('body').addClass('accessible');
                $(document).off('keydown');

            }
        });

        $('.to-top-btn').click(function(e){
            e.preventDefault();
            window.scrollTo({top: 0, behavior: 'smooth'});
        });

        //change stylesets
        var $stylesetButtons = $('.btn-styleset');
        $stylesetButtons.click(function(e){
            e.preventDefault();
            $stylesetButtons.removeClass('active');
            $(this).addClass('active');
            var ss = $(this).attr('data-style');
            $textInput.removeClass('ss01 ss02 ss03 ss04 ss05 ss06').addClass(ss);
        });

        //change fontcase
        var $caseButtons = $('.btn-case');
        $caseButtons.click(function(e){
            e.preventDefault();
            $caseButtons.removeClass('active');
            $(this).addClass('active');
            var fontcase = $(this).attr('data-style');
            if(fontcase == 'text-uppercase'){
                $textInput.removeClass('text-lowercase').addClass(fontcase);
            } else if(fontcase == 'text-lowercase'){
                $textInput.removeClass('text-uppercase').addClass(fontcase);
            }
        });

        //animate the glyphs
        glyphs = $('.glyph-char').attr('data-content');
        glypsArray = glyphs.split(',');
        ref.shuffleArray(glypsArray);
        glyph_index = 0;
        ref.setGlyph();
        glyphs_timer = setInterval(ref.setGlyph, 750);
        $('.glyphs-btn').click(function(){
            if(glyphs_timer) clearInterval(glyphs_timer);
            setTimeout(function(){
                if(glyphs_timer) clearInterval(glyphs_timer);
                glyphs_timer = setInterval(ref.setGlyph, 750);
            }, 500);
            ref.setGlyph();
        });

        $textTickers = $('.ticker');
        if($('html').hasClass('touch')){
            $textTickers[0].addEventListener('touchstart', function(e){
                ref.animateElement($(this).find('.ticker-target'));
            }, false);
        } else {
            $textTickers.on('mouseenter', function(){
                ref.animateElement($(this).find('.ticker-target'));
            }).on('mouseleave', function(){
                ref.resetElement();
            });;
        }

        $changeFontButtons = $('.btn-changefont');
        $changeFontButtons.click(function(e){
            e.preventDefault();
            $changeFontButtons.removeClass('active');
            $textInput.removeClass().addClass($(this).attr('data-class'));
            $textInput.removeClass('ss01 ss02 ss03 ss04 ss05 ss06').addClass('ss01');
            $stylesetButtons.first().trigger('click');
            $(this).addClass('active');
            $('.text-input-box').attr('data-type',$(this).attr('data-type'))
        });

        currentSpecimenIndex = 0;
        var $downloadSpecimen = $('.download-specimen');
        if($('html').hasClass('touch')){
            $downloadSpecimen[0].addEventListener('touchstart', function(e){
                ref.animateSpecimen($(this).find('svg'));
            }, false);
            $downloadSpecimen[0].addEventListener('touchend', function(e){
                ref.resetSpecimen($(this).find('svg'));
            }, false);
        } else {
            $downloadSpecimen.on('mouseenter', function(){
                ref.animateSpecimen($(this).find('svg'));
            }).on('mouseleave', function(){
                ref.resetSpecimen($(this).find('svg'));
            });
        }

        var $flipElement = $('.flip-element');
        if($('html').hasClass('touch')){
            $flipElement[0].addEventListener('touchstart', function(e){
                if($(this).hasClass('elem-1')){
                    $(this).removeClass('elem-1').addClass('elem-2');
                } else {
                    $(this).removeClass('elem-2').addClass('elem-1');
                }
            }, false);
        } else {
            $flipElement[0].addEventListener('click', function(e){
                if($(this).hasClass('elem-1')){
                    $(this).removeClass('elem-1').addClass('elem-2');
                } else {
                    $(this).removeClass('elem-2').addClass('elem-1');
                }
            }, false);
        }

        var div = document.getElementById("text-input");
        div.onfocus = function() {
            window.setTimeout(function() {
                var sel, range;
                if (window.getSelection && document.createRange) {
                    range = document.createRange();
                    range.selectNodeContents(div);
                    sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
                } else if (document.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(div);
                    range.select();
                }
            }, 1);
        };

        //enable mobile navigation on logo click
        if(ref.viewport().width < 1024){
            $('.logo')[0].addEventListener('touchstart', function(e){
                e.preventDefault();
                blockRemove = true;
                setTimeout(function(){
                    blockRemove = false;
                }, 100);
                $topNav.addClass('visible');
            });
        }


        //flip claim on click
        clearTimeout(claim_timer);

        $('.hero').click(function(){
            ref.setClaim();
        });


        //start claim ticker
        claim_timer = setTimeout(function(){
            ref.setClaim();
        }, 8000);

        ref.addEventHandlers();


        $('.loader').addClass('gone');
        setTimeout(function(){
            $('.loader').remove();
        }, 1000);


    };

    Controller.prototype.setClaim = function(){
        clearTimeout(claim_timer);
        var claim = claims[claimsIndex];
        Logger.log("setClaim: index -> ", claimsIndex);
        Logger.log("setClaim: -> ", claim);
        var $span = $('<span></span>').html(claim.text).addClass(claim.size).attr('style',claim.style).addClass('gone');
        var styleIndex = ref.getRandomNumberBetween(0, claimStyles.length-1);
        Logger.log(claimStyles);
        var styleset = claimStyles[styleIndex];
        $span.addClass(styleset.style);
        $('.hero-inner').empty().append($span);
        setTimeout(function(){
            $span.removeClass('gone');
        }, 100);


        if(claimsIndex < claims.length -1 ){
            claimsIndex++;
        } else claimsIndex = 0;


        Logger.log("claimsIndex", claimsIndex);
        Logger.log("claims.length", claims.length);

        claim_timer = setTimeout(function(){
            $('.hero-inner').removeClass('text-center');
            ref.setClaim();
        }, claim.delay);

    }

    Controller.prototype.animateSpecimen = function($elem){
        specimen_timer = setInterval(function(){
            var color = ref.specimen_colors[currentSpecimenIndex];
            $elem.find('path').css({ fill: color });
            if(currentSpecimenIndex < ref.specimen_colors.length -1 ){
                currentSpecimenIndex++;
            } else currentSpecimenIndex = 0;
        }, 20);
    }

    Controller.prototype.resetSpecimen = function($elem){
        $elem.find('path').css({ fill: '#000' });
        if(specimen_timer) clearInterval(specimen_timer);

    }


    Controller.prototype.animateElement = function($elem){
        ref.resetElement();
        $elem.attr('data-original', $elem.text());
        currentTickerIndex = 0;
        ticker_timer = setInterval(function(){
            var current = $elem.text();
            current = current.split('');
            var first = current.shift();
            current.push(first.toString());
            current = current.join("");
            //Logger.log(current);
            $elem.text(current);
            if(currentTickerIndex < $elem.attr('data-original').length){
                currentTickerIndex++;
            } else currentTickerIndex = 0;
        }, 20);
        $currentTickerElement = $elem;
    }

    Controller.prototype.resetElement = function($elem){
        if(ticker_timer) clearInterval(ticker_timer);
        if($currentTickerElement) $currentTickerElement.text($currentTickerElement.attr('data-original'));
    }

    Controller.prototype.setGlyph = function(){
        $('.glyph-char').text(glypsArray[glyph_index]);
        if(glyph_index < glypsArray.length-1){
            glyph_index++;
        } else glyph_index = 0;
    }

    Controller.prototype.addEventHandlers = function(){


        /*********************
         scroll event
         *********************/
        lastScrollTop = 0;
        $(window).scroll(function(event){

            if(ref.viewport().width < 1024 && blockRemove == false){
                if($topNav.hasClass('visible')){
                    $topNav.removeClass('visible');
                }
            }
            if($header.hasClass('mobile-nav-open')){
                $header.removeClass('mobile-nav-open');
            }

            //reset the ticker animations
            ref.resetElement();

            var st = $(this).scrollTop();
            var hh = $header.height();

            if (st > lastScrollTop){
                // down
                if(st > 0){
                    //remove header
                    $header.addClass('fixed gone');
                }
            } else {
                // up
                if($header.hasClass('gone')){
                    //show header
                    $header.removeClass('gone');
                }
                if(st == hh) $header.removeClass('fixed');
            }
            lastScrollTop = st;

        });

        /*********************
         resize event
         *********************/
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $(window).resize(function () {
            delay(function () {
                ref.resize();
            }, 50);
        });
        ref.resize();
    };

    /*********************
     resize event handler
     *********************/
    Controller.prototype.resize = function(){
        $(".hero").height(window.innerHeight + "px");
    };

    /*********************
     viewport().width, viewport().height
     *********************/
    Controller.prototype.viewport = function()
    {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    };

    /*********************
     get browser type + version
     *********************/
    Controller.prototype.getBrowser = function()
    {
        var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name:'IE',version:(tem[1]||'')};
        }
        if(M[1]==='Chrome'){
            tem=ua.match(/\bOPR\/(\d+)/)
            if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }
        M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
        return {
            name: M[0],
            version: M[1]
        };
    };

    /*********************
     get random int in range
     *********************/
    Controller.prototype.getRandomNumberBetween = function(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    /*********************
     shuffle array
     *********************/
    Controller.prototype.shuffleArray = function(array)
    {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    window.Controller = Controller;

}(window));